# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta
from .account import weight_mixin
from trytond.modules.product import price_digits


class SaleLine(weight_mixin('sale'), metaclass=PoolMeta):
    __name__ = 'sale.line'

    @classmethod
    def __setup__(cls):
        super(SaleLine, cls).__setup__()
        if hasattr(cls, 'costs_amount'):
            cls.costs_weight_price = fields.Function(
                fields.Numeric('Costs weight price', digits=price_digits),
                'get_costs_weight_price')

    def get_invoice_line(self):
        lines = super(SaleLine, self).get_invoice_line()
        for line in lines:
            line.price_unit = self.price_unit
            line.weight_uom = self.weight_uom
            if self.weight:
                factor = self.quantity / line.quantity
                line.weight = self.weight_uom.round(
                    self.weight * factor)
        return lines

    @classmethod
    def get_costs_weight_price(cls, records, name=None):
        return cls._get_costs_price(records, 'weight')
