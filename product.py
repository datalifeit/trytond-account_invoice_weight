# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta
from trytond.model import ModelSQL, fields
from trytond.pyson import Eval
from trytond.modules.company.model import CompanyValueMixin


class Template(metaclass=PoolMeta):
    __name__ = 'product.template'

    price_unit = fields.MultiValue(fields.Selection([
        ('weight', 'Weight'),
        ('uom', 'UOM')], 'Price unit',
        states={'readonly': ~Eval('active')},
        depends=['active']))
    price_units = fields.One2Many('product.template.price_unit',
        'template', 'Product units')

    @classmethod
    def default_price_unit(cls, **pattern):
        return cls.multivalue_model('price_unit').default_price_unit()


class ProductPriceUnit(ModelSQL, CompanyValueMixin):
    """ Product Price Unit"""
    __name__ = 'product.template.price_unit'

    price_unit = fields.Selection([
        ('weight', 'Weight'),
        ('uom', 'UOM')], 'Price unit')
    template = fields.Many2One('product.template', 'Template',
        ondelete='CASCADE', select=True)

    @staticmethod
    def default_price_unit():
        return 'uom'


class Product(metaclass=PoolMeta):
    __name__ = 'product.product'
