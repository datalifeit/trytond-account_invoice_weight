datalife_account_invoice_weight
===============================

The account_invoice_weight module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-account_invoice_weight/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-account_invoice_weight)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
